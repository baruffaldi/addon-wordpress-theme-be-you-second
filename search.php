<?php get_header(); ?>
<?php include("inc/functions.inc.php"); ?>
<div id="content">
		

	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
				<div class="content_inner" id="post-<?php the_ID(); ?>">
				
			    <div class="left">
				<div class="left_single">
				<table>
				<thead><tr><th colspan="3"></th></tr></thead>
				<tbody>
					<tr><td class="author"><span>Author: <?php the_author_firstname(); ?></span> </td></tr>
					<tr class="odd"><td class="feed"><span><?php comments_rss_link('Comment feeds'); ?></span> </td></tr>
					<tr><td><?php the_time('l, F jS, Y') ?> at <?php the_time() ?></td></tr>
					<tr class="odd"><td><?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Both Comments and Pings are open ?>
							You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(true); ?>" rel="trackback">trackback</a> from your own site.
						
						<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Only Pings are Open ?>
							Responses are currently closed, but you can <a href="<?php trackback_url(true); ?> " rel="trackback">trackback</a> from your own site.
						
						<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Comments are open, Pings are not ?>
							You can skip to the end and leave a response. Pinging is currently not allowed.
			
						<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Neither Comments, nor Pings are open ?>
							Both comments and pings are currently closed.			
						
						<?php } ?></td></tr>
						<tr><td>Category: <?php the_category(', '); ?></td></tr>
				</tbody>
				</table>




				
				</div><!--end left_info"-->
			</div><!--end left-->
			
			<h1 class="headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
			<div class="right right_single">
			<?php the_content('Read the rest of this entry &raquo;'); ?>
			</div><!--end right column-->
			
		</div><!--end content_inner-->
		
		<?php comments_template(); ?>
						<p class="hr1"><i>line</i></p>
				
				
					
				
		
				
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Previous Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Next Entries &raquo;') ?></div>
		</div>
		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>



<?php get_footer(); ?>
