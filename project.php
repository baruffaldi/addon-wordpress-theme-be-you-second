<?php
/*
Template Name: Projects
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/nonsense" href="http://www.alvinwoon.com/blog/wp-content/themes/alvin/safaricss.css" />
		        <!--[if IE]>
                <link rel="stylesheet" type="text/css" href="http://www.alvinwoon.com/blog/wp-content/themes/alvin/ieonly.css" />
                <![endif]-->
	<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/alvinwoon" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="shortcut icon" href="http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico" />
		<meta name="robots" content="all" />
		<meta http-equiv="imagetoolbar" content="false" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		<meta name="author" content="Alvin Woon W. P. - alvinwoon(at)gmail.com" />
		<meta name="Copyright" content="Copyright (c) 2002-2006 Alvin Woon" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<script type="text/javascript" src="http://alvinwoon.com/blog/wp-content/themes/alvin/inc/sortable_tables.js"></script>
		<script type="text/javascript" src="http://alvinwoon.com/blog/wp-content/themes/alvin/inc/MochiKit.js"></script>

<?php wp_head(); ?>
</head>
<body>
<div id="container">
	<div id="head"><?php include (TEMPLATEPATH . '/searchform.php'); ?>
		 <ul id="menu">
 			 <li id="menu_1"><a href="http://alvinwoon.com/blog/" title="home"></a></li>
 			 <li id="menu_2"><a href="http://alvinwoon.com/blog/collophon/" title="about me"></a></li>
 			 <li id="menu_3"><a href="http://alvinwoon.com/blog/project" title="project"></a></li>
 			 <li id="menu_4"><a href="http://alvinwoon.com/blog/archives/" title="archive"></a></li>
			 <li id="menu_5"><a href="http://alvinwoon.com/blog/contact-me" title="contact me"></a></li>
			 <li id="menu_6"><a href="http://alvinwoon.com/blog/daily-link/" title="interesting daily links"></a></li>
			 <li id="menu_7"><a href="/slideshow/slideshow.html" title="photo gallery"></a></li>
			 <li id="rss"><a href="http://feeds.feedburner.com/alvinwoon" title="rss feeds"></a></li>
 		</ul>
	</div><!--end head-->
	
<?php include("inc/functions.inc.php"); ?>
<div id="content">
		

	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
				<div class="content_inner" id="post-<?php the_ID(); ?>">
				
			    <div class="left">
				<p class="left_image"><?php echo c2c_get_custom('image', '', '', ''); ?></p>
				<div class="left_single">
				<table>
				<thead><tr><th colspan="3"></th></tr></thead>
				<tbody>
					<tr><td class="archive_notesworthy"><span><a href="http://alvinwoon.com/blog/movies/" title="movielicious">Movielicious</a></span> </td></tr>
					<tr class="odd"><td class="comment1"><span><a href="http://alvinwoon.com/blog/project" title="playground">Playground</a></span> </td></tr>
					<tr><td class="comment2"><span><a href="#" title="technews">Tech news</a></span></td></tr>

				</tbody>
				</table>




				
				</div><!--end left_info"-->
			</div><!--end left-->
			<h1 class="headline"><a href="#" rel="bookmark" title="Archive"><?php the_title(); ?></a></h1>
			<div class="right right_single">
			<?php the_content('Read the rest of this entry &raquo;'); ?>
</div><!--end right column-->
			
		</div><!--end content_inner-->

						<p class="hr1"><i>line</i></p>
				
				
					
				
		
				
		<?php endwhile; ?>

		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>



<?php get_footer(); ?>
