<?php
	/*
	 * Title: functions.inc.php
	 *	Global functions for EVOK Interactive
	 */

	/*
	 * Function: bubble_date
	 *
	 * Parameters:
	 * 	strDate - The date to parse
	 * Returns:
	 * 	a string containing the bubble class housing the date	
	 */
	function bubble_date($strDate) {
		$strMonth = substr($strDate,0,3);
		$strDay   = substr($strDate,3,6);

		return ('<p class="left_date"><span class="date">' . $strMonth . '</span><br /><span class="date">'. $strDay . '</span></p>');
	}




?>
