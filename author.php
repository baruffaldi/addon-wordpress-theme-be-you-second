<?php
/*
Template Name: author
*/
?>
<?php get_header(); ?>
<?php include("inc/functions.inc.php"); ?>
<div id="content">
		

	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
				<div class="content_inner" id="post-<?php the_ID(); ?>">
				
			    <div class="left">
				<p class="left_image"><?php echo c2c_get_custom('image', '', '', ''); ?></p>
				<div class="left_single">
				<table>
				<thead><tr><th colspan="3"></th></tr></thead>
				<tbody>
					<tr><td class="author"><span>Alvin Woon W. P.</span> </td></tr>
					<tr class="odd"><td class="country"><span>Malaysian</span> </td></tr>
					<tr><td>23 years old</td></tr>
					<tr class="odd"><td>Hobby: reading, coding, music &amp; movie, soccer, play with my belly hair.</td></tr>

				</tbody>
				</table>




				
				</div><!--end left_info"-->
			</div><!--end left-->
			<h1 class="headline"><a href="http://127.0.0.1/alvin/index.php/about/" rel="bookmark" title="Permanent Link to About">About Alvin Woon</a></h1>
			<div class="right right_single">
			<p><ul class="normal">
<li>I founded <a href="http://puppylab.com/index.php">Puppy Lab</a>, a web media development studio based in Lincoln NE.</li>
<li>Currently working as a Programmer Analyst Specialist for <a href="http://www.unl.edu/">University of Nebraska Lincoln</a>.</li>
<li>I <span class="strike">am currently a senior computer science student</span> recently graduated from the University of Nebraska-Lincoln with a degree in <span class="strike">Counter Strike</span> Computer Science.</li>
<li>Living in Lincon, NE right now. No puppy, <span class="strike">one girlfriend</span>, half decent food, no nothing.</li>
<li>Sleeping and computer programming are my favorite leisure activities.</li>

<li>I seem to look sleepy most of the time, and when I am deprived of a good sleep, I&#8217;m very easily amused.</li>
<li>I believe strongly in Open Source, p2p, web standards, pacificism, karma, well-done sirloin, extra-terrestrial life and free porn without pop-ups.</li>
<li>I believe web standard is the way to go. But I&#8217;m in no way one of those Mac-preaching, flickr-whoring, CSS-shouting and <span class="strike">shadow-page-blog-design</span> type of person. Having said that, I do love <a href="http://www.zeldman.com">Jeffrey Zeldman</a>.</li>
<li>I love cheesecake with raspberry toppings, watermelon, dark beer, Jessica Simpson&#8217;s breasts and ultra-silent keyboard.</li>
</ul>

</div>
<br />
<h1 class="headline"><a title="about daily misery" href="http://127.0.0.1/alvin/wp-admin/post.php#">About Daily Misery</a></h1>
<div class="right"><p>DM has been around for quite some time now. It is a place where I write about my various web projects, cheesecakes, program codes and daily life.</p>

<p>The contents on this site are my own personal opinions and do not represent my employer's view in any ways. </p>
</div><!--end right column-->
			
		</div><!--end content_inner-->

						<p class="hr1"><i>line</i></p>
				
				
					
				
		
				
		<?php endwhile; ?>

		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>



<?php get_footer(); ?>
