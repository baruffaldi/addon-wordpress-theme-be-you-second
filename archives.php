<?php
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>
<?php include("inc/functions.inc.php"); ?>
<div id="content">
		

	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
				<div class="content_inner" id="post-<?php the_ID(); ?>">
				
			    <div class="left">
				<p class="left_image"><?php echo c2c_get_custom('image', '', '', ''); ?></p>
				<div class="left_single">
				<table>
				<thead><tr><th colspan="3"></th></tr></thead>
				<tbody>
					<tr><td class="archive_notesworthy"><span><a href="http://alvinwoon.com/blog/noteworthy" title="noteworthy">Noteworthy</a></span> </td></tr>
					<tr class="odd"><td class="comment1"><span><a href="http://alvinwoon.com/blog/most_recent_comments" title="most recent comments">Recent comments</a></span> </td></tr>
					<tr><td class="comment2"><span><a href="http://alvinwoon.com/blog/most_commented" title="most commented posts">Most commented</a></span></td></tr>
					<tr class="odd"><td class="comment3"><span><a href="http://alvinwoon.com/blog/weighted_categories" title="weighted categories">Category weight</a></span></td></tr>

				</tbody>
				</table>




				
				</div><!--end left_info"-->
			</div><!--end left-->
			<h1 class="headline"><a href="#" rel="bookmark" title="Archive"><?php the_title(); ?></a></h1>
			<div class="right right_single">
			<p><?php the_content('Read the rest of this entry &raquo;'); ?></p>
</div><!--end right column-->
			
		</div><!--end content_inner-->

						<p class="hr1"><i>line</i></p>
				
				
					
				
		
				
		<?php endwhile; ?>

		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>



<?php get_footer(); ?>
